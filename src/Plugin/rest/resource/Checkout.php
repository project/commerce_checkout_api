<?php

namespace Drupal\commerce_checkout_api\Plugin\rest\resource;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_checkout_api\SupportHeadlessPaymentInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides a resource to checkout a commerce order.
 *
 * @RestResource(
 *   id = "commerce_checkout_api_checkout",
 *   label = @Translation("Checkout commerce order"),
 *   uri_paths = {
 *     "create" = "/api/rest/commerce-checkout-api/checkout"
 *   }
 * )
 */
class Checkout extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new PaymentResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('commerce_checkout_api'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * @param array $data
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(array $data) {

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->isAuthenticated()) {
      throw new AccessDeniedHttpException('没有登录');
    }

    // 检查参数
    if (!isset($data['gateway'])) {
      throw new BadRequestHttpException('没有指定网关：gateway');
    }
    if (!isset($data['order'])) {
      throw new BadRequestHttpException('没有指定订单：order');
    }

    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = \Drupal::service('entity_type.manager')->getStorage('commerce_payment_gateway');
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $payment_gateway_storage->load($data['gateway']);
    if (!($payment_gateway instanceof PaymentGatewayInterface)) {
      throw new BadRequestHttpException('指定的网关['.$data['gateway'].']不存在！');
    }

    /** @var EntityRepositoryInterface $entity_repository */
    $entity_repository = \Drupal::service('entity.repository');
    /** @var OrderInterface $commerce_order */
    $commerce_order = Order::load($data['order']);
    if (!$commerce_order) $commerce_order = $entity_repository->loadEntityByUuid('commerce_order', $data['order']);
    if (!$commerce_order) throw new BadRequestHttpException('指定的订单['.$data['order'].']不存在！');
    if ($commerce_order->getCustomerId() !== $this->currentUser->id())
      throw new BadRequestHttpException('订单['.$data['order'].']不属于当前用户['.$this->currentUser->getAccountName().']！');


    // 验证并保存支付网关
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway) {
      throw new BadRequestHttpException('无效的支付网关');
    }
    if (!($payment_gateway_plugin instanceof SupportHeadlessPaymentInterface)) {
      throw new BadRequestHttpException('指定的支付网关不支持此调用方式。');
    }
    $commerce_order->set('payment_gateway', $payment_gateway);
    if ($commerce_order->getTotalPrice()->isZero() && $commerce_order->getState()->getId() === 'draft') $commerce_order->getState()->applyTransitionById('place');

    // 把订单从购物车检出，并保存订单
    /** @var CartProviderInterface $cart_provider */
    $cart_provider = \Drupal::service('commerce_cart.cart_provider');
    $cart_provider->finalizeCart($commerce_order, true);

    // 生成支付配置数据
    $config_data = null;
    try {
      if (!$commerce_order->getTotalPrice()->isZero())
        $config_data = $payment_gateway_plugin->getClientLaunchConfig($commerce_order);
    } catch (\Exception $exception) {
      throw new BadRequestHttpException($exception->getMessage());
    }

    return new ModifiedResourceResponse($config_data, 200);
  }
}
