<?php
namespace Drupal\commerce_checkout_api;

use  \Drupal\commerce_order\Entity\OrderInterface;

interface SupportHeadlessPaymentInterface
{
  public function getClientLaunchConfig(OrderInterface $commerce_order);
}
